package com.tbc.ge;


import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;

import com.codeborne.selenide.ElementsCollection;
import io.qameta.allure.Description;
import org.junit.Test;

public class ChallengingDOMTest {
    @Test
    @Description("Verifies if table's first column elements ended by 0")
    // Couldn't get all elements from first column

    public void challengingDOMTest() {
        open("http://the-internet.herokuapp.com");
        $(byText("Challenging DOM")).click();
        ElementsCollection firstColElements = $$(byXpath("//*/tbody/tr/td[1]"));
        firstColElements.contains(text("0"));

    }
}