package com.tbc.ge;

import io.qameta.allure.Description;
import org.junit.Test;

import static com.codeborne.selenide.Condition.cssValue;
import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selectors.withText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class ShiftingContentTest {


    @Test
    @Description("Verifies if 'Home' design changes")
    public void shiftingContentHomeBlockChange() {

        open("http://the-internet.herokuapp.com");
        $(byText("Shifting Content")).click();
        $(withText("Menu Element")).click();
        $(byAttribute("href", "/")).hover();
        $(byAttribute("href", "/"))
                .shouldHave(cssValue("font-size", "20px"));
    }

    //    დავალებაში ეწერა ფოტოს ზომა თუ იცვლებოდა და მოცემულ გვერდზე ფოტოს ზომას არაფერი ცვლის,
//    მარტო პოზიცია იცვლებოდა და მაგაზე დავწერე ტესტი
    @Test
    @Description("Verifies if image position changes with clicking 2nd 'Click Here'")

    public void shiftingContentImagePositionChange() {
        open("http://the-internet.herokuapp.com");
        $(byText("Shifting Content")).click();
        $(withText("An image")).click();
        $("a", 2).click();
        $(".shift").shouldHave(cssValue("left", "-100px"));
    }

}
