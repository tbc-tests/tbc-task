package com.tbc.ge;


import io.qameta.allure.Description;
import org.junit.Test;

import java.io.File;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class UserFileUploadTest {


    @Test
    @Description("Uploads file and then verifies if file is uploaded successfully")

    public void userFileUpload() {

        open("http://the-internet.herokuapp.com");
        $(byText("File Upload")).click();
        $("input#file-upload").uploadFile(
                new File("file.png")
        );
        $("input.button").click();
        $("#uploaded-files").shouldHave(text("file.png"));
    }
}
